package com.example.springbootbasicscodalong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class SpringBootBasicsCodalongApplicationTests {

    @Autowired
    FlowersController flowersController;

    @Autowired
    FlowerRepository flowerRepository;

    FlowerEntity dandelion;
    FlowerEntity sunFlower;

    @BeforeEach
    void setUp() {
        dandelion = flowerRepository.save(new FlowerEntity(
                "Dandelion",
                "Yellow"));
        sunFlower = flowerRepository.save(new FlowerEntity(
                "Sunflower",
                "Green"));
    }

    @Test
    void test_all_flowers_success() {
        List<FlowerDTO> all = flowersController.all();

        List<FlowerDTO> wantedList = Stream.of(sunFlower, dandelion)
                .map(FlowersController::toDTO)
                .collect(Collectors.toList());

        assertTrue(
                wantedList.size() == all.size()
                        && wantedList.containsAll(all)
                        && all.containsAll(wantedList));
    }

    @Test
    void test_create_flower_success() {
        FlowerDTO blomma = flowersController.create(new CreateFlower("Blomma"));

        assertEquals("Blomma", blomma.getName());
        assertEquals("Orange", blomma.getColor());
    }
}
