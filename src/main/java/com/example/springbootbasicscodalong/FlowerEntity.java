package com.example.springbootbasicscodalong;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
public class FlowerEntity {
    String id;
    String name;
    String color;

    public FlowerEntity(String name, String color) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.color = color;
    }
}
