package com.example.springbootbasicscodalong;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/flowers")
@AllArgsConstructor
public class FlowersController {

    StandardFlowerService flowerService;

    @GetMapping
    public List<FlowerDTO> all() {
        return flowerService.all().stream()
                .map(FlowersController::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping
    public FlowerDTO create(@RequestBody CreateFlower createFlower) {
        return toDTO(
                flowerService.create(createFlower.getName()));
    }

    public static FlowerDTO toDTO(FlowerEntity flowerEntity) {
        return new FlowerDTO(
                flowerEntity.id,
                flowerEntity.getName(),
                flowerEntity.getColor()
        );
    }
}
