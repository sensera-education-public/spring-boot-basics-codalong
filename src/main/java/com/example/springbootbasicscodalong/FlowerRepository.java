package com.example.springbootbasicscodalong;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FlowerRepository {

    Map<String,FlowerEntity> flowers = new HashMap<>();

    public List<FlowerEntity> all() {
        return new ArrayList<>(flowers.values());
    }

    public FlowerEntity save(FlowerEntity flowerEntity) {
        flowers.put(flowerEntity.getId(), flowerEntity);
        return flowerEntity;
    }

}
