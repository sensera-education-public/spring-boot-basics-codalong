package com.example.springbootbasicscodalong;

import java.util.List;

public interface FlowerService {
    FlowerEntity create(String name);
    List<FlowerEntity> all();
}
