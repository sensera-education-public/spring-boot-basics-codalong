package com.example.springbootbasicscodalong;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StandardFlowerService implements FlowerService {

    FlowerRepository flowerRepository;
    ColorConfiguration colorConfiguration;

    public List<FlowerEntity> all() {
        return flowerRepository.all();
    }

    public FlowerEntity create(String name) {
        FlowerEntity flowerEntity = new FlowerEntity(name, colorConfiguration.getInitialColor());
        return flowerRepository.save(flowerEntity);
    }
}
