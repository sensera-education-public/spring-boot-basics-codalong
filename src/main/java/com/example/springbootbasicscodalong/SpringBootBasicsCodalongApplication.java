package com.example.springbootbasicscodalong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBasicsCodalongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBasicsCodalongApplication.class, args);
    }

}
