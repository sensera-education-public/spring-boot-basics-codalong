package com.example.springbootbasicscodalong;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateFlower {
    String name;

    @JsonCreator
    public CreateFlower(@JsonProperty("name") String name) {
        this.name = name;
    }
}
