package com.example.springbootbasicscodalong;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("prod")
@AllArgsConstructor
public class ProdConfiguration {

    FlowerRepository flowerRepository;
    ColorConfiguration colorConfiguration;

    @Bean FlowerService flowerService() {
        return new ProdFlowerService(flowerRepository, colorConfiguration);
    }
}
