package com.example.springbootbasicscodalong;

import lombok.Value;

@Value
public class FlowerDTO {
    String id;
    String name;
    String color;
}
