package com.example.springbootbasicscodalong;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!prod")
@AllArgsConstructor
public class StandardConfiguration {

    FlowerRepository flowerRepository;
    ColorConfiguration colorConfiguration;

    @Bean FlowerService flowerService() {
        return new StandardFlowerService(flowerRepository, colorConfiguration);
    }
}
